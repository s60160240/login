/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.Serializable;

/**
 *
 * @author informatics
 */
public class user implements Serializable{
    private String username;
    private String password;
    private String fristname;
    private String lastname;
    private String tel;
    
    public user(String username, String password, String fristname, String lastname, String tel) {
        this.username = username;
        this.password = password;
        this.fristname = fristname;
        this.lastname = lastname;
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "user{" + "username=" + username + ", password=" + password + ", fristname=" + fristname + ", lastname=" + lastname + ", tel=" + tel + '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFristname() {
        return fristname;
    }

    public void setFristname(String fristname) {
        this.fristname = fristname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    
}
